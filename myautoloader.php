<?php

defined("__MAIN__") or exit;

class MyAutoloader {

    public static function autoload($class) {
        $_class = __DIR__ . "/cls/" . str_replace("\\", "/", $class) . ".php";
        if (file_exists($_class))
            require_once $_class;
    }

}

spl_autoload_register('MyAutoloader::autoload');
