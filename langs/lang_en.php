<?php

defined("__MAIN__") or exit;

$lang = [];
$lang['registration'] = 'Registration';
$lang['general'] = 'General';
$lang['place_of_living'] = 'Place of living';
$lang['education'] = 'Education';
$lang['work_experience'] = 'Work experience';
$lang['contacts'] = 'Contacts';
$lang['credentials'] = 'Credentials';
$lang['about_me'] = 'About me';
$lang['login'] = 'Log in';

$lang['firstname'] = 'First name';
$lang['firstname_placeholder'] = 'Enter your first name';
$lang['firstname_is_empty'] = 'First name is empty';

$lang['lastname'] = 'Last name';
$lang['lastname_placeholder'] = 'Enter your last name';
$lang['lastname_is_empty'] = 'Last name is empty';

$lang['patronymic'] = 'Patronymic';
$lang['patronymic_placeholder'] = 'Enter your patronymic';
$lang['patronymic_is_empty'] = 'Patronymic is empty';

$lang['birthdate'] = 'Birth date';
$lang['birthdate_placeholder'] = 'Enter your birthdate';
$lang['birthdate_is_empty'] = 'Birthdate is empty';

$lang['marital_status'] = 'Marital status';
$lang['marital_status_select'] = 'Select marital status';

$lang['country'] = 'Country';
$lang['country_select'] = 'Select country';

$lang['city'] = 'City/State';
$lang['city_placeholder'] = 'Enter city/state where you live';

$lang['address'] = 'Address';
$lang['address_placeholder'] = 'Enter address of your living place';

$lang['university'] = 'University';
$lang['university_placeholder'] = 'Enter university';

$lang['department'] = 'Department';
$lang['department_placeholder'] = 'Enter department';

$lang['degree'] = 'Degree';
$lang['degree_placeholder'] = 'Select degree';

$lang['start_year'] = 'Start year';
$lang['start_year_placeholder'] = 'Enter start year';

$lang['end_year'] = 'End year';
$lang['end_year_placeholder'] = 'Enter end year';

$lang['company'] = 'Company';
$lang['company_placeholder'] = 'Enter company name';

$lang['position'] = 'Position at company';
$lang['position_placeholder'] = 'Enter position at company';

$lang['current_work'] = 'Still working There';

$lang['phone'] = 'Phone(s)';
$lang['phone_placeholder'] = 'Enter your phone. If more than one then separate them with comma';

$lang['email'] = 'E-mail';
$lang['email_placeholder'] = 'Enter your e-mail';

$lang['linkedin'] = 'Linked in';
$lang['linkedin_placeholder'] = 'Enter your linkedin if exists';

$lang['github'] = 'Github';
$lang['github_placeholder'] = 'Enter your github if exists';

$lang['additional_information'] = 'Additional information';
$lang['additional_information_placeholder'] = 'Enter additional information about yourself if you have something to add';

$lang['username'] = 'Username';
$lang['username_placeholder'] = 'Enter username';
$lang['username_exists'] = 'Username belongs to other person. Enter different username';

$lang['password'] = 'Password';
$lang['password_placeholder'] = 'Enter password';
$lang['current_password'] = 'Current password';
$lang['new_password'] = 'New Password';
$lang['change_password'] = 'Change Password';
$lang['type_all_passwords'] = 'Current password or new password cannot be empty';
$lang['current_password_error'] = 'Current password is wrong';


$lang['back'] = 'Back';
$lang['next'] = 'Next';
$lang['complete'] = 'Complete';

$lang['profile_photo'] = 'Profile photo';

$lang['incorrect_url'] = 'Incorrect url';

$lang['file_error'] = "File was not uploaded";
$lang['file_isnot_image'] = "File is not image";
$lang['file_size_error'] = "File size is bigger than allowed. Allowed size is ";
$lang['file_type_error'] = "File is not allowed image. Allowed image types are JPG, GIF, PNG";
$lang['file_dim_error'] = "Image width and height should be ";
$lang['credential_error'] = 'Username or password is wrong';
$lang['logout'] = 'Log out';
