<?php

defined("__MAIN__") or exit;

$lang = [];
$lang['registration'] = 'Регистрация';
$lang['general'] = 'Главная информация';
$lang['place_of_living'] = 'Место жительства';
$lang['education'] = 'Образование';
$lang['work_experience'] = 'Опыт работы';
$lang['contacts'] = 'Kонтакты';
$lang['credentials'] = 'Полномочия';
$lang['about_me'] = 'Обо мне';
$lang['login'] = 'Войти';

$lang['firstname'] = 'Имя';
$lang['firstname_placeholder'] = 'Введите свое имя';
$lang['firstname_is_empty'] = 'Имя пустое';

$lang['lastname'] = 'Фамилия';
$lang['lastname_placeholder'] = 'Введите свою фамилию';
$lang['lastname_is_empty'] = 'Фамилия пуста';

$lang['patronymic'] = 'Отчество';
$lang['patronymic_placeholder'] = 'Введите свое отчество';
$lang['patronymic_is_empty'] = 'Отчество пусто';

$lang['birthdate'] = 'Дата рождения';
$lang['birthdate_placeholder'] = 'Введите дату рождения';
$lang['birthdate_is_empty'] = 'Дата рождения пуста';

$lang['marital_status'] = 'Семейное положение';
$lang['marital_status_select'] = 'Выберите семейное положение';

$lang['country'] = 'Страна';
$lang['country_select'] = 'Выберите страну';

$lang['city'] = 'Город / Область';
$lang['city_placeholder'] = 'Введите город / штат, в котором вы живете';

$lang['address'] = 'Адрес';
$lang['address_placeholder'] = 'Введите адрес своего места жительства';

$lang['university'] = 'Университет';
$lang['university_placeholder'] = 'Введите название университета';

$lang['department'] = 'Факультет';
$lang['department_placeholder'] = 'Введите название факультета';

$lang['degree'] = 'Cтепень';
$lang['degree_placeholder'] = 'Выберите степень';

$lang['start_year'] = 'начальный год';
$lang['start_year_placeholder'] = 'начальный год пуст';

$lang['end_year'] = 'Конечный год';
$lang['end_year_placeholder'] = 'Конечный год пуст';

$lang['company'] = 'Компания';
$lang['company_placeholder'] = 'Введите название компании';

$lang['position'] = 'Должность в компании';
$lang['position_placeholder'] = 'Введите должность в компании';

$lang['current_work'] = 'Все еще работю там';

$lang['phone'] = 'Телефон (ы)';
$lang['phone_placeholder'] = 'Введите свой телефон. Если более одного, то разделите их запятой';

$lang['email'] = 'E-mail';
$lang['email_placeholder'] = 'ведите свой e-mail';

$lang['linkedin'] = 'Linked in';
$lang['linkedin_placeholder'] = 'Введите свой linkedin если есть';

$lang['github'] = 'Github';
$lang['github_placeholder'] = 'Введите свой github если есть';

$lang['additional_information'] = 'Дополнительная информация';
$lang['additional_information_placeholder'] = 'Введите дополнительную информацию о вас, если есть что добавить';

$lang['username'] = 'Имя пользователя';
$lang['username_placeholder'] = 'Введите имя пользователя';
$lang['username_exists'] = 'Имя пользователя принадлежит другому лицу. Введите другое имя пользователя';

$lang['password'] = 'Пароль';
$lang['password_placeholder'] = 'Введите пароль';
$lang['current_password'] = 'Текущий пароль';
$lang['new_password'] = 'Новый пароль';
$lang['change_password'] = 'Изменить пароль';
$lang['type_all_passwords'] = 'Текущий пароль или новый пароль не могут быть пустыми';
$lang['current_password_error'] = 'Неверный пароль';


$lang['back'] = 'Назад';
$lang['next'] = 'Следующий';
$lang['complete'] = 'Завершать регистрацию';

$lang['profile_photo'] = 'Аватар';

$lang['incorrect_url'] = 'Неверный URL';

$lang['file_error'] = "Файл не был загружен";
$lang['file_isnot_image'] = "Файл не изображение";
$lang['file_size_error'] = "Размер файла больше, чем разрешено. Допустимый размер ";
$lang['file_type_error'] = "Файл не допускается. Допустимые типы изображений: JPG, GIF, PNG";
$lang['file_dim_error'] = "Ширина и высота изображения должны быть ";
$lang['credential_error'] = 'Неправильное имя пользователя или пароль';
$lang['logout'] = 'Выход';
