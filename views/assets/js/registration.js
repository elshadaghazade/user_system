"use strict";

(function ($) {
    // checking if jquery was defined
    if (typeof $ == 'undefined')
        return;

    // Object prototype conflicts with jQuery, that's why I've defined my own "on" method with this way
    // my custom event listener
    Object.defineProperty(Object.prototype, 'on', {
        value: function (action, callback) {
            if (typeof action != 'string')
                throw 'Event action name was not defined';

            if (typeof callback != 'function')
                throw 'Event callback function was not defined';

            if (this.removeEventListener)
                this.removeEventListener(action, callback, false);

            if (this.addEventListener)
                this.addEventListener(action, callback);
        }
    });
    // my custom fire event method
    Object.defineProperty(Object.prototype, 'fire', {
        value: function (action) {
            if (typeof action != 'string')
                throw 'Action name was not defined';

            if (this.fireEvent) {
                this.fireEvent('on' + action);
            } else {
                var evObj = document.createEvent('Events');
                evObj.initEvent(action, true, false);
                this.dispatchEvent(evObj);
            }
        }
    });


    // check if form exists
    var form = document.querySelectorAll('.registraion-form');
    if (!form.length)
        return;

    // on form submit fire next button click
    form[0].on('submit', function (e) {
        e.preventDefault();

        document.querySelector('.next-btn').fire('click');
    });

    var steps = form[0].querySelectorAll('.step');
    var step_cnt = steps.length;
    // start from general info form
    var current_step = 0;

    init_steps();

    document.querySelector('.back-btn').on('click', function () {});

    // initialize steps of registration
    function init_steps() {
        steps.forEach(function (el) {
            el.style.display = 'none';
        });
        // display first step
        steps[current_step].style.display = 'block';

        // construct breadcrumb
        create_breadcrumb();
        // update buttons
        update_buttons();
        // username existance checking enable
        enable_check_username();
        // enable photo renderer
        enable_photo_renderer();
        // enable next button click
        enable_next_button_click();
    }

    // enable next button click
    function enable_next_button_click() {
        // bind click event to prev/next buttons
        document.querySelectorAll('.back-btn,.next-btn').forEach(function (el) {
            el.on('click', function () {
                if (this.className.match('next-btn')) {
                    if (check_form(current_step)) {
                        activate_step(current_step + 1, true);
                    }
                } else {
                    activate_step(current_step - 1);
                }

                update_buttons();
            });
        });
    }

    // photo renderer
    function enable_photo_renderer() {
        form[0].querySelector('[name=photo]').on('change', function (e) {
            var file = this;
            if (file && file.files[0].type.match(/^image/i)) {
                var reader = new FileReader();
                reader.readAsDataURL(file.files[0]);
                reader.onload = function () {
                    var img = new Image();
                    img.src = reader.result;
                    document.querySelector('.profile_photo_wrapper').style.display = 'block';
                    document.querySelector('.profile_photo_wrapper').innerHTML = '';
                    document.querySelector('.profile_photo_wrapper').appendChild(img);
                }
            }

        });
    }

    // check username
    function enable_check_username() {
        var timeout;
        var timedelay = 500;
        form[0].querySelector('[name=username]').on('input', function () {
            var username = this.value;

            // allow send ajax request with 0.5sec interval
            if (timeout)
                clearTimeout(timeout);

            timeout = setTimeout(function () {
                $.ajax({
                    url: '?pg=ajax',
                    data: {
                        method: 'user_exists',
                        username: username
                    },
                    dataType: 'json',
                    type: 'POST',
                    success: function (res) {
                        if (res.status != 200)
                            return;

                        if (res.data == 1) {
                            form[0].querySelector('#username').dataset.username = 'exists';
                            $(form).find('[name=username]').parent('.form-group').addClass('has-error');
                            $(form).find('[name=username]').parent('.form-group').addClass('has-feedback');
                            $(form).find('[name=username]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
                            $(form).find('[name=username]').after('<span class="help-block">' + lang['username_exists'] + '</span>');
                        } else {
                            form[0].querySelector('[name=username]').dataset.username = 'free';
                            $(form).find('[name=username]').parent('.form-group').removeClass('has-error');
                            $(form).find('[name=username]').parent('.form-group').removeClass('has-feedback');
                            $(form).find('[name=username]').next('.glyphicon-remove').remove();
                            $(form).find('[name=username]').next('.help-block').remove();
                        }
                    }
                });
            }, timedelay);
        });
    }

    // update buttons like back/next buttons, breadcrumb buttons and etc.
    function update_buttons() {
        // hide back button when we are on first step
        if (current_step == 0)
            document.querySelector('.back-btn').style.display = 'none';
        else
            document.querySelector('.back-btn').style.display = 'block';

        if (current_step == step_cnt - 1)
            document.querySelector('.next-btn').innerHTML = lang['complete'];
        else
            document.querySelector('.next-btn').innerHTML = lang['next'] + ' &raquo;';

        // bind click event to breadcrumb items
        document.querySelectorAll('.breadcrumb li').forEach(function (el) {
            el.on('click', function () {
                activate_step(this.dataset.nth);
            });
        });

        // update active breadcrumb
        document.querySelectorAll('.breadcrumb li').forEach(function (el) {
            el.className = el.className.replace('active', '');
        });

        document.querySelectorAll('.breadcrumb li')[current_step].className = ' active';
    }

    // creates breadcrumb buttons according to steps of registration
    function create_breadcrumb() {
        var bc = document.querySelector('.breadcrumb');
        bc.innerHTML = '';

        steps.forEach(function (el, ind) {
            var title = el.querySelector('legend').innerHTML;
            var li = document.createElement('li');
            li.dataset.nth = ind;
            li.className = !ind ? 'active' : '';
            li.innerHTML = (ind + 1) + ' ' + title;
            bc.appendChild(li);
        });
    }

    // switchs to next/prev steps. If force params is false or undefined then
    // withought checking form we don't allow to switch next step
    function activate_step(step, force) {
        if (step > (step_cnt - 1))
            step = step_cnt - 1;
        else if (step < 0)
            step = 0;

        if (!force) {
            if (step >= current_step)
                return;
            else if (step < current_step)
                current_step = step;
        } else {
            current_step = step;
        }


        steps.forEach(function (el) {
            el.style.display = 'none';
        });
        steps[step].style.display = 'block';
        update_buttons();
    }

    // this function calls appropriate form checker by step number
    function check_form(step) {
        var fd = new FormData(form[0]);
        document.querySelectorAll('.form-group').forEach(function (el) {
            el.className = el.className.replace(/has-error|has-feedback/, '');
            try {
                el.querySelector('.glyphicon-remove').remove();
                el.querySelector('.help-block').remove();
            } catch (E) {

            }
        });

        switch (step) {
            case 0: // first step: General info
                return check_general_form(fd);
            case 1: // second step: Place of living
                return check_place_of_living_form(fd);
                break;
            case 2: // third step: Education
                return check_education_form(fd);
            case 3: // fourth step: Work experience
                return check_work_experience_form(fd);
            case 4: // fifth step: contacts
                return check_contacts_form(fd);
            case 5: // additional information
                return true;
            case 6: // credentials
                return check_credentials_form(fd);

        }
    }

    // check General info form
    function check_general_form(fd) {
        var has_error = false;

        if (!fd.get('firstname').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=firstname]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=firstname]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=firstname]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=firstname]').after('<span class="help-block">' + lang['firstname_is_empty'] + '</span>');
        }

        if (!fd.get('lastname').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=lastname]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=lastname]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=lastname]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=lastname]').after('<span class="help-block">' + lang['lastname_is_empty'] + '</span>');
        }

        if (!fd.get('patronymic').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=patronymic]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=patronymic]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=patronymic]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=patronymic]').after('<span class="help-block">' + lang['patronymic_is_empty'] + '</span>');
        }

        if (!fd.get('birthdate').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=birthdate]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=birthdate]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=birthdate]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=birthdate]').after('<span class="help-block">' + lang['birthdate_is_empty'] + '</span>');
        }

        if (!fd.get('marital_status').trim()) {
            has_error = true;
            $('[name=marital_status]').parent('.form-group').addClass('has-error');
            $('[name=marital_status]').parent('.form-group').addClass('has-feedback');
            $('[name=marital_status]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $('[name=marital_status]').after('<span class="help-block">' + lang['marital_status_select'] + '</span>');
        }


        return !has_error;
    }

    // check place of living form
    function check_place_of_living_form(fd) {
        var has_error = false;

        if (!fd.get('country').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=country]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=country]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=country]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=country]').after('<span class="help-block">' + lang['country_select'] + '</span>');
        }

        if (!fd.get('city').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=city]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=city]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=city]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=city]').after('<span class="help-block">' + lang['city_placeholder'] + '</span>');
        }

        if (!fd.get('address').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=address]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=address]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=address]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=address]').after('<span class="help-block">' + lang['address_placeholder'] + '</span>');
        }

        return !has_error;
    }

    // check education form
    function check_education_form(fd) {
        var has_error = false;

        if (!fd.get('edu_country').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=edu_country]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=edu_country]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=edu_country]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=edu_country]').after('<span class="help-block">' + lang['country_select'] + '</span>');
        }

        if (!fd.get('edu_city').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=edu_city]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=edu_city]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=edu_city]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=edu_city]').after('<span class="help-block">' + lang['city_placeholder'] + '</span>');
        }

        if (!fd.get('department').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=department]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=department]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=department]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=department]').after('<span class="help-block">' + lang['department_placeholder'] + '</span>');
        }

        if (!fd.get('degree').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=degree]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=degree]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=degree]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=degree]').after('<span class="help-block">' + lang['degree_placeholder'] + '</span>');
        }

        if (!fd.get('edu_start_year').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=edu_start_year]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=edu_start_year]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=edu_start_year]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=edu_start_year]').after('<span class="help-block">' + lang['start_year_placeholder'] + '</span>');
        }

        if (!fd.get('edu_end_year').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=edu_end_year]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=edu_end_year]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=edu_end_year]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=edu_end_year]').after('<span class="help-block">' + lang['end_year_placeholder'] + '</span>');
        }

        return !has_error;
    }

    // check work experience form
    function check_work_experience_form(fd) {
        var has_error = false;

        if (!fd.get('work_country').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=work_country]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=work_country]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=work_country]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=work_country]').after('<span class="help-block">' + lang['country_select'] + '</span>');
        }

        if (!fd.get('work_city').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=work_city]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=work_city]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=work_city]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=work_city]').after('<span class="help-block">' + lang['city_placeholder'] + '</span>');
        }

        if (!fd.get('company').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=company]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=company]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=company]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=company]').after('<span class="help-block">' + lang['company_placeholder'] + '</span>');
        }

        if (!fd.get('position').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=position]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=position]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=position]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=position]').after('<span class="help-block">' + lang['position_placeholder'] + '</span>');
        }

        if (!fd.get('work_start_year').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=work_start_year]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=work_start_year]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=work_start_year]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=work_start_year]').after('<span class="help-block">' + lang['start_year_placeholder'] + '</span>');
        }

        if (!fd.get("working") && !fd.get('work_end_year').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=work_end_year]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=work_end_year]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=work_end_year]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=work_end_year]').after('<span class="help-block">' + lang['end_year_placeholder'] + '</span>');
        }

        return !has_error;
    }

    // check contacts form
    function check_contacts_form(fd) {
        var has_error = false;

        if (!fd.get('phone').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=phone]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=phone]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=phone]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=phone]').after('<span class="help-block">' + lang['phone_placeholder'] + '</span>');
        }

        if (!fd.get('email').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=email]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=email]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=email]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=email]').after('<span class="help-block">' + lang['email_placeholder'] + '</span>');
        }

        return !has_error;
    }

    // check credentials form
    function check_credentials_form(fd) {
        var has_error = false;

        if (!fd.get('username').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=username]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=username]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=username]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=username]').after('<span class="help-block">' + lang['username_placeholder'] + '</span>');
        } else if (steps[current_step].querySelector('[name=username]').dataset.username == 'exists') {
            $(form).find('[name=username]').parent('.form-group').addClass('has-error');
            $(form).find('[name=username]').parent('.form-group').addClass('has-feedback');
            $(form).find('[name=username]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(form).find('[name=username]').after('<span class="help-block">' + lang['username_exists'] + '</span>');
            has_error = true;
        }

        if (!fd.get('password').trim()) {
            has_error = true;
            $(steps).eq(current_step).find('[name=password]').parent('.form-group').addClass('has-error');
            $(steps).eq(current_step).find('[name=password]').parent('.form-group').addClass('has-feedback');
            $(steps).eq(current_step).find('[name=password]').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            $(steps).eq(current_step).find('[name=password]').after('<span class="help-block">' + lang['password_placeholder'] + '</span>');
        }

        if (has_error)
            return false;

        register();
    }

    // register with ajax
    function register() {
        var fd = new FormData(form[0]);
        fd.set('method', 'register');

        $.ajax({
            url: '?pg=ajax',
            data: fd,
            dataType: 'json',
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            success: function (res) {
                if (res.status != 200) {
                    process_error(res.error);
                    return;
                }

                window.location.href = '?pg=login';
            },
            complete: function (res) {
                console.log(res);
            }
        });
    }

    function process_error(error) {
        if (typeof error.step == 'undefined')
            return;

        $(form).eq(error.step).find('[name=' + error.field + ']').parent('.form-group').addClass('has-error');
        $(steps).eq(error.step).find('[name=' + error.field + ']').parent('.form-group').addClass('has-feedback');
        $(steps).eq(error.step).find('[name=' + error.field + ']').after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
        $(steps).eq(error.step).find('[name=' + error.field + ']').after('<span class="help-block">' + error.error + '</span>');

        activate_step(error.step, true);
    }

})(typeof jQuery != 'undefined' ? jQuery : undefined);