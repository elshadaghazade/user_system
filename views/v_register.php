<?php
defined("__MAIN__") or exit();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registration</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="views/assets/css/style.css"/>
        <script>
            var lang = <?= json_encode($lang) ?>
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <h2 class="header"><?= $lang['registration'] ?> /
                <a href="?pg=<?= \libs\utils::http_var("pg") ?>&lang=<?= $this->lang == 'en' ? 'ru' : 'en' ?>"><?= $this->lang == 'en' ? 'russian' : 'english' ?></a></h2>
        </h2>
        <ul class="breadcrumb custom-width-breadcrumb"></ul>
        <form class="form-horizontal registraion-form" action="" method="post" enctype="multipart/form-data">
            <div class="row step custom-width">
                <br>
                <fieldset>
                    <legend><?= $lang['general'] ?>:</legend>
                    <div class="row">
                        <div class="form-group">
                            <div class="profile_photo_wrapper"></div>
                            <label class="control-label" for="photo"><?= $lang['profile_photo'] ?>:</label>
                            <input type="file" class="form-control" id="photo" placeholder="<?= $lang['profile_photo'] ?>" name="photo">
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="firstname"><?= $lang['firstname'] ?>:</label>
                            <input type="text" class="form-control" id="firstname" placeholder="<?= $lang['firstname_placeholder'] ?>" name="firstname">
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="firstname"><?= $lang['lastname'] ?>:</label>
                            <input type="text" class="form-control" id="lastname" placeholder="<?= $lang['lastname_placeholder'] ?>" name="lastname">
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="fathername"><?= $lang['patronymic'] ?>:</label>
                            <input type="text" class="form-control" id="patronymic" placeholder="<?= $lang['patronymic_placeholder'] ?>" name="patronymic">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-4" style="padding-left:0;">
                            <label class="control-label" for="birthdate"><?= $lang['birthdate'] ?>:</label>
                            <input type="date" class="form-control" id="birthdate" placeholder="mm/dd/yyyy" name="birthdate">
                        </div>
                        <div class="form-group col-lg-4" style="padding-left:40px;">
                            <label class="control-label" for="marital_status"><?= $lang['marital_status'] ?>:</label>
                            <select name="marital_status" id="marital_status" class="form-control">
                                <option value="">-- <?= $lang['marital_status_select'] ?> --</option>
                                <?php foreach ($config['marital_status'] as $key => $status): ?>
                                    <option value="<?= $key ?>"><?= $status ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="row step custom-width">
                <br>
                <fieldset>
                    <legend><?= $lang['place_of_living'] ?>:</legend>
                    <div class="form-group">
                        <label class="control-label" for="country"><?= $lang['country'] ?>:</label>
                        <select name="country" id="country" class="form-control">
                            <option value="">-- <?= $lang['country_select'] ?> --</option>
                            <?php foreach ($config['countries'] as $key => $country): ?>
                                <option value="<?= $key ?>"><?= $country['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="city"><?= $lang['city'] ?>:</label>
                        <input type="text" name="city" id="city" class="form-control" placeholder="<?= $lang['city_placeholder'] ?>"> 
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="address"><?= $lang['address'] ?>:</label>
                        <input type="text" name="address" id="address" class="form-control" placeholder="<?= $lang['address_placeholder'] ?>"> 
                    </div>
                </fieldset>
            </div>
            <div class="row step custom-width">
                <br>
                <fieldset>
                    <legend>Education:</legend>
                    <div class="form-group">
                        <label class="control-label" for="edu_country"><?= $lang['country'] ?>:</label>
                        <select name="edu_country" id="edu_country" class="form-control">
                            <option value="">-- <?= $lang['country_select'] ?> --</option>
                            <?php foreach ($config['countries'] as $key => $country): ?>
                                <option value="<?= $key ?>"><?= $country['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="edu_city"><?= $lang['city'] ?>:</label>
                        <input type="text" name="edu_city" id="edu_city" class="form-control" placeholder="<?= $lang['city_placeholder'] ?>"> 
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="university"><?= $lang['university'] ?>:</label>
                        <input type="text" name="university" id="university" class="university form-control" placeholder="<?= $lang['university_placeholder'] ?>"> 
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="department"><?= $lang['department'] ?>:</label>
                        <input type="text" name="department" id="department" class="form-control" placeholder="<?= $lang['department_placeholder'] ?>"> 
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="degree"><?= $lang['degree'] ?>:</label>
                        <select name="degree" id="country" class="degree form-control">
                            <option value="">-- <?= $lang['degree_placeholder'] ?> --</option>
                            <?php foreach ($config['degree'] as $key => $degree): ?>
                                <option value="<?= $key ?>"><?= $degree['en'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group col-lg-4" style="padding-left:0;">
                        <label class="control-label" for="edu_start_year"><?= $lang['start_year'] ?>:</label>
                        <input min="0" type="number" name="edu_start_year" id="edu_start_year" class="department form-control" placeholder="<?= $lang['start_year_placeholder'] ?>"> 
                    </div>
                    <div class="form-group col-lg-4" style="padding-left: 40px;">
                        <label class="control-label" for="edu_end_year"><?= $lang['end_year'] ?>:</label>
                        <input min="0" type="number" name="edu_end_year" id="edu_end_year" class="department form-control" placeholder="<?= $lang['end_year_placeholder'] ?>"> 
                    </div>
                </fieldset>
            </div>
            <div class="row step custom-width">
                <br>
                <fieldset>
                    <legend><?= $lang['work_experience'] ?>:</legend>
                    <div class="form-group">
                        <label class="control-label" for="work_country"><?= $lang['country'] ?>:</label>
                        <select name="work_country" id="edu_country" class="form-control">
                            <option value="">-- <?= $lang['country_select'] ?> --</option>
                            <?php foreach ($config['countries'] as $key => $country): ?>
                                <option value="<?= $key ?>"><?= $country['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="work_city"><?= $lang['city'] ?>:</label>
                        <input type="text" name="work_city" id="edu_city" class="form-control" placeholder="<?= $lang['city_placeholder'] ?>"> 
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="company"><?= $lang['company'] ?>:</label>
                        <input type="text" name="company" id="company" class="university form-control" placeholder="<?= $lang['company_placeholder'] ?>"> 
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="position"><?= $lang['position'] ?>:</label>
                        <input type="text" name="position" id="position" class="form-control" placeholder="<?= $lang['position_placeholder'] ?>"> 
                    </div>
                    <div class="form-group col-lg-4" style="padding-left:0;">
                        <label class="control-label" for="work_start_year"><?= $lang['start_year'] ?>:</label>
                        <input min="0" type="number" name="work_start_year" id="work_start_year" class="department form-control" placeholder="<?= $lang['start_year_placeholder'] ?>"> 
                    </div>
                    <div class="form-group col-lg-4" style="padding-left: 40px;">
                        <label class="control-label" for="work_end_year"><?= $lang['end_year'] ?>:</label>
                        <input min="0" type="number" name="work_end_year" id="work_end_year" class="department form-control" placeholder="<?= $lang['end_year_placeholder'] ?>"> 
                    </div>
                    <div class="form-group col-lg-4" style="padding-left: 40px;">
                        <label class="control-label" for="working"><input type="checkbox" id="working" onchange="$('#work_end_year').prop('disabled', $(this).is(':checked'))" name="working"> <?= $lang['current_work'] ?></label>
                    </div>
                </fieldset>
            </div>
            <div class="row step custom-width">
                <br>
                <fieldset>
                    <legend><?= $lang['contacts'] ?>:</legend>
                    <div class="form-group">
                        <label class="control-label" for="phone"><?= $lang['phone'] ?>:</label>
                        <input type="text" name="phone" id="phone" class="form-control" placeholder="<?= $lang['phone_placeholder'] ?>"> 
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="email"><?= $lang['email'] ?>:</label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="<?= $lang['email_placeholder'] ?>"> 
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="linkedin"><?= $lang['linkedin'] ?>:</label>
                        <input type="url" name="linkedin" id="email" class="form-control" placeholder="<?= $lang['linkedin_placeholder'] ?>"> 
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="github"><?= $lang['github'] ?>:</label>
                        <input type="url" name="github" id="github" class="form-control" placeholder="<?= $lang['github_placeholder'] ?>"> 
                    </div>
                </fieldset>
            </div>
            <div class="row step custom-width">
                <br>
                <fieldset>
                    <legend><?= $lang['about_me'] ?>:</legend>
                    <div class="form-group">
                        <label class="control-label" for="additional_information"><?= $lang['additional_information'] ?>:</label>
                        <textarea maxlength="1000" name="additional_information" class="form-control" rows="15" id="additional_information" placeholder="<?= $lang['additional_information_placeholder'] ?>"></textarea>
                    </div>
                </fieldset>
            </div>
            <div class="row step custom-width">
                <br>
                <fieldset>
                    <legend><?= $lang['credentials'] ?>:</legend>
                    <div class="form-group">
                        <label class="control-label" for="username"><?= $lang['username'] ?>:</label>
                        <input type="text" name="username" id="username" data-username="free" class="form-control" placeholder="<?= $lang['username_placeholder'] ?>"> 
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="password"><?= $lang['password'] ?>:</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="<?= $lang['password_placeholder'] ?>"> 
                    </div>
                </fieldset>
            </div>
            <div class="row custom-width">
                <div class="col-lg-6">
                    <button type="button" class="form-control btn btn-primary back-btn">&laquo; <?= $lang['back'] ?></button>
                </div>
                <div class="col-lg-6">
                    <button type="button" class="form-control btn btn-primary next-btn"><?= $lang['next'] ?> &raquo;</button>
                </div>
            </div>
            <input type="submit" style="display: none">
        </form>
    </div>
    <script src="views/assets/js/registration.js"></script>
</body>
</html>
