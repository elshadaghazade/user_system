<?php
defined("__MAIN__") or exit();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= $user['firstname'] ?> <?= $user['lastname'] ?> <?= $user['patronymic'] ?></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="views/assets/css/style.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <h2 class="header">
                <?= $user['firstname'] ?> <?= $user['lastname'] ?> <?= $user['patronymic'] ?> /
                <a href="?pg=<?= \libs\utils::http_var("pg") ?>&lang=<?= $this->lang == 'en' ? 'ru' : 'en' ?>"><?= $this->lang == 'en' ? 'russian' : 'english' ?></a></h2>
            <form class="form-horizontal registraion-form" action="" method="post" enctype="multipart/form-data">
                <div class="row step custom-width">
                    <br>
                    <fieldset>
                        <legend><?= $lang['general'] ?>:</legend>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="profile_photo_wrapper" style="display:block;float: left;"><img src="?pg=media&type=profile"></div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <a href="?pg=logout" style="position:absolute;right: 0;top:0;font-size:1.2em;"><?= $lang['logout'] ?></a>
                                <div class="form-group">
                                    <label class="control-label" for="firstname"><?= $lang['firstname'] ?>:</label>
                                    <?= $user['firstname'] ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="firstname"><?= $lang['lastname'] ?>:</label>
                                    <?= $user['lastname'] ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="fathername"><?= $lang['patronymic'] ?>:</label>
                                    <?= $user['patronymic'] ?>
                                </div>
                                <div class="form-group" style="padding-left:0;">
                                    <label class="control-label" for="birthdate"><?= $lang['birthdate'] ?>:</label>
                                    <?= date("d M, Y", strtotime($user['birthdate'])) ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="marital_status"><?= $lang['marital_status'] ?>:</label>
                                    <?= $config['marital_status'][$user['marital_status']] ?>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="row step custom-width">
                    <br>
                    <fieldset>
                        <legend><?= $lang['place_of_living'] ?>:</legend>
                        <div class="form-group">
                            <label class="control-label" for="country"><?= $lang['country'] ?>:</label>
                            <?= $config['countries'][$user['country']]['name'] ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="city"><?= $lang['city'] ?>:</label>
                            <?= $user['city'] ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="address"><?= $lang['address'] ?>:</label>
                            <?= $user['address'] ?>
                        </div>
                    </fieldset>
                </div>
                <div class="row step custom-width">
                    <br>
                    <fieldset>
                        <legend>Education:</legend>
                        <div class="form-group">
                            <label class="control-label" for="edu_country"><?= $lang['country'] ?>:</label>
                            <?= $config['countries'][$education['country']]['name'] ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="edu_city"><?= $lang['city'] ?>:</label>
                            <?= $education['city'] ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="university"><?= $lang['university'] ?>:</label>
                            <?= $education['university'] ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="department"><?= $lang['department'] ?>:</label>
                            <?= $education['department'] ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="degree"><?= $lang['degree'] ?>:</label>
                            <?= $config['degree'][$education['degree']]['en'] ?>
                        </div>
                        <div class="form-group col-lg-4" style="padding-left:0;">
                            <label class="control-label" for="edu_start_year"><?= $lang['start_year'] ?>:</label>
                            <?= $education['start_year'] ?>
                        </div>
                        <div class="form-group col-lg-4" style="padding-left: 40px;">
                            <label class="control-label" for="edu_end_year"><?= $lang['end_year'] ?>:</label>
                            <?= $education['end_year'] ?>
                        </div>
                    </fieldset>
                </div>
                <div class="row step custom-width">
                    <br>
                    <fieldset>
                        <legend><?= $lang['work_experience'] ?>:</legend>
                        <div class="form-group">
                            <label class="control-label" for="work_country"><?= $lang['country'] ?>:</label>
                            <?= $config['countries'][$work_experience['country']]['name'] ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="work_city"><?= $lang['city'] ?>:</label>
                            <?= $work_experience['city'] ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="company"><?= $lang['company'] ?>:</label>
                            <?= $work_experience['company'] ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="position"><?= $lang['position'] ?>:</label>
                            <?= $work_experience['position'] ?>
                        </div>
                        <div class="form-group col-lg-4" style="padding-left:0;">
                            <label class="control-label" for="work_start_year"><?= $lang['start_year'] ?>:</label>
                            <?= $work_experience['start_year'] ?>
                        </div>
                        <div class="form-group col-lg-4" style="padding-left: 40px;">
                            <label class="control-label" for="work_end_year"><?= $lang['end_year'] ?>:</label>
                            <?= $work_experience['working'] ? "continuing" : $work_experience['end_year'] ?>
                        </div>
                    </fieldset>
                </div>
                <div class="row step custom-width">
                    <br>
                    <fieldset>
                        <legend><?= $lang['contacts'] ?>:</legend>
                        <div class="form-group">
                            <label class="control-label" for="phone"><?= $lang['phone'] ?>:</label>
                            <?= $user['phone'] ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="email"><?= $lang['email'] ?>:</label>
                            <?= $user['email'] ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="linkedin"><?= $lang['linkedin'] ?>:</label>
                            <?= $user['linkedin'] ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="github"><?= $lang['github'] ?>:</label>
                            <?= $user['github'] ?>
                        </div>
                    </fieldset>
                </div>
                <div class="row step custom-width">
                    <br>
                    <fieldset>
                        <legend><?= $lang['about_me'] ?>:</legend>
                        <div class="form-group">
                            <label class="control-label" for="additional_information"><?= $lang['additional_information'] ?>:</label>
                            <?= $user['about_me'] ?>
                        </div>
                    </fieldset>
                </div>
                <div class="row step custom-width">
                    <br>
                    <fieldset>
                        <legend><?= $lang['credentials'] ?>:</legend>
                        <div class="form-group">
                            <label class="control-label" for="username"><?= $lang['username'] ?>:</label>
                            <?= $user['username'] ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="current_password"><?= $lang['current_password'] ?>:</label>
                            <input type="password" name="current_password" id="current_password" class="form-control" placeholder="<?= $lang['current_password'] ?>"> 
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="new_password"><?= $lang['new_password'] ?>:</label>
                            <input type="password" name="new_password" id="new_password" class="form-control" placeholder="<?= $lang['new_password'] ?>"> 
                        </div>
                    </fieldset>
                </div>
                <div class="row custom-width">
                    <div class="col-lg-12">
                        <button type="submit" name="change_password" value="1" class="btn btn-primary back-btn"><?= $lang['change_password'] ?></button>
                    </div>
                </div>
                <input type="submit" style="display: none">
            </form>
        </div>
    </body>
</html>
