<?php
defined("__MAIN__") or exit();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registration</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="views/assets/css/style.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row main custom-width-login">
                <div class="panel-heading">
                    <div class="panel-title text-center">
                        <h1 class="title"><?= $lang['login'] ?> /
                        <a href="?pg=<?= \libs\utils::http_var("pg")?>&lang=<?= $this->lang == 'en' ? 'ru' : 'en'?>"><?=$this->lang == 'en' ? 'russian' : 'english'?></a></h2>
                        </h1>
                        <hr />
                    </div>
                </div> 
                <div class="main-login main-center">
                    <?php if (!empty($this->error)): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <?= $this->error ?>
                        </div>
                    <?php endif; ?>
                    <form class="form-horizontal" method="post" action="">

                        <div class="form-group">
                            <label for="username" class="cols-sm-2 control-label"><?= $lang['username'] ?></label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="username" id="name"  placeholder="<?= $lang['username_placeholder'] ?>"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="cols-sm-2 control-label"><?= $lang['password'] ?></label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    <input type="password" class="form-control" name="password" id="password"  placeholder="<?= $lang['password_placeholder'] ?>"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <button type="submit" name="login" value="1" class="btn btn-primary btn-lg btn-block login-button"><?= $lang['login'] ?></button>
                        </div>
                        <div class="login-register">
                            <a href="?pg=register"><?= $lang['registration'] ?></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script src="views/assets/js/registration.js"></script>
    </body>
</html>
