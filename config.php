<?php

defined("__MAIN__") or exit;

$config = [];

define("CONF_SESS_COOKIE_NAME", "_TSKSESS_");
define("CONF_SESS_EXPIRE_TIME", 86400);
define("CONF_SESS_DOMAIN", "");

//sql config
$config['sql_user'] = 'root';
$config['sql_pass'] = 'root';
$config['sql_db'] = 'test_task';
$config['sql_host'] = 'localhost';
$config['sql_port'] = '3306';
$config['sql_socket'] = '';

// allowed image types
$config['allowed_media_types'] = [
    IMAGETYPE_JPEG => [
        'ext' => 'jpg',
        'mime' => 'image/jpeg',
        'type' => IMAGETYPE_JPEG,
    ],
    IMAGETYPE_PNG => [
        'ext' => 'png',
        'mime' => 'image/png',
        'type' => IMAGETYPE_PNG,
    ],
    IMAGETYPE_GIF => [
        'ext' => 'gif',
        'mime' => 'image/gif',
        'type' => IMAGETYPE_GIF,
    ]
];

$config['profile_photo'] = [
    'size' => 200 * 1024,
    'site_str' => '200KB',
    'width' => 400,
    'height' => 300
];

$config["countries"] = [
    1 => ['code' => 'AF', 'name' => 'Afghanistan'],
    2 => ['code' => 'AL', 'name' => 'Albania'],
    3 => ['code' => 'DZ', 'name' => 'Algeria'],
    4 => ['code' => 'DS', 'name' => 'American Samoa'],
    5 => ['code' => 'AD', 'name' => 'Andorra'],
    6 => ['code' => 'AO', 'name' => 'Angola'],
    7 => ['code' => 'AI', 'name' => 'Anguilla'],
    8 => ['code' => 'AQ', 'name' => 'Antarctica'],
    9 => ['code' => 'AG', 'name' => 'Antigua and Barbuda'],
    10 => ['code' => 'AR', 'name' => 'Argentina'],
    11 => ['code' => 'AM', 'name' => 'Armenia'],
    12 => ['code' => 'AW', 'name' => 'Aruba'],
    13 => ['code' => 'AU', 'name' => 'Australia'],
    14 => ['code' => 'AT', 'name' => 'Austria'],
    15 => ['code' => 'AZ', 'name' => 'Azerbaijan'],
    16 => ['code' => 'BS', 'name' => 'Bahamas'],
    17 => ['code' => 'BH', 'name' => 'Bahrain'],
    18 => ['code' => 'BD', 'name' => 'Bangladesh'],
    19 => ['code' => 'BB', 'name' => 'Barbados'],
    20 => ['code' => 'BY', 'name' => 'Belarus'],
    21 => ['code' => 'BE', 'name' => 'Belgium'],
    22 => ['code' => 'BZ', 'name' => 'Belize'],
    23 => ['code' => 'BJ', 'name' => 'Benin'],
    24 => ['code' => 'BM', 'name' => 'Bermuda'],
    25 => ['code' => 'BT', 'name' => 'Bhutan'],
    26 => ['code' => 'BO', 'name' => 'Bolivia'],
    27 => ['code' => 'BA', 'name' => 'Bosnia and Herzegovina'],
    28 => ['code' => 'BW', 'name' => 'Botswana'],
    29 => ['code' => 'BV', 'name' => 'Bouvet Island'],
    30 => ['code' => 'BR', 'name' => 'Brazil'],
    31 => ['code' => 'IO', 'name' => 'British Indian Ocean Territory'],
    32 => ['code' => 'BN', 'name' => 'Brunei Darussalam'],
    33 => ['code' => 'BG', 'name' => 'Bulgaria'],
    34 => ['code' => 'BF', 'name' => 'Burkina Faso'],
    35 => ['code' => 'BI', 'name' => 'Burundi'],
    36 => ['code' => 'KH', 'name' => 'Cambodia'],
    37 => ['code' => 'CM', 'name' => 'Cameroon'],
    38 => ['code' => 'CA', 'name' => 'Canada'],
    39 => ['code' => 'CV', 'name' => 'Cape Verde'],
    40 => ['code' => 'KY', 'name' => 'Cayman Islands'],
    41 => ['code' => 'CF', 'name' => 'Central African Republic'],
    42 => ['code' => 'TD', 'name' => 'Chad'],
    43 => ['code' => 'CL', 'name' => 'Chile'],
    44 => ['code' => 'CN', 'name' => 'China'],
    45 => ['code' => 'CX', 'name' => 'Christmas Island'],
    46 => ['code' => 'CC', 'name' => 'Cocos (Keeling) Islands'],
    47 => ['code' => 'CO', 'name' => 'Colombia'],
    48 => ['code' => 'KM', 'name' => 'Comoros'],
    49 => ['code' => 'CG', 'name' => 'Congo'],
    50 => ['code' => 'CK', 'name' => 'Cook Islands'],
    51 => ['code' => 'CR', 'name' => 'Costa Rica'],
    52 => ['code' => 'HR', 'name' => 'Croatia (Hrvatska)'],
    53 => ['code' => 'CU', 'name' => 'Cuba'],
    54 => ['code' => 'CY', 'name' => 'Cyprus'],
    55 => ['code' => 'CZ', 'name' => 'Czech Republic'],
    56 => ['code' => 'DK', 'name' => 'Denmark'],
    57 => ['code' => 'DJ', 'name' => 'Djibouti'],
    58 => ['code' => 'DM', 'name' => 'Dominica'],
    59 => ['code' => 'DO', 'name' => 'Dominican Republic'],
    60 => ['code' => 'TP', 'name' => 'East Timor'],
    61 => ['code' => 'EC', 'name' => 'Ecuador'],
    62 => ['code' => 'EG', 'name' => 'Egypt'],
    63 => ['code' => 'SV', 'name' => 'El Salvador'],
    64 => ['code' => 'GQ', 'name' => 'Equatorial Guinea'],
    65 => ['code' => 'ER', 'name' => 'Eritrea'],
    66 => ['code' => 'EE', 'name' => 'Estonia'],
    67 => ['code' => 'ET', 'name' => 'Ethiopia'],
    68 => ['code' => 'FK', 'name' => 'Falkland Islands (Malvinas)'],
    69 => ['code' => 'FO', 'name' => 'Faroe Islands'],
    70 => ['code' => 'FJ', 'name' => 'Fiji'],
    71 => ['code' => 'FI', 'name' => 'Finland'],
    72 => ['code' => 'FR', 'name' => 'France'],
    73 => ['code' => 'FX', 'name' => 'France, Metropolitan'],
    74 => ['code' => 'GF', 'name' => 'French Guiana'],
    75 => ['code' => 'PF', 'name' => 'French Polynesia'],
    76 => ['code' => 'TF', 'name' => 'French Southern Territories'],
    77 => ['code' => 'GA', 'name' => 'Gabon'],
    78 => ['code' => 'GM', 'name' => 'Gambia'],
    79 => ['code' => 'GE', 'name' => 'Georgia'],
    80 => ['code' => 'DE', 'name' => 'Germany'],
    81 => ['code' => 'GH', 'name' => 'Ghana'],
    82 => ['code' => 'GI', 'name' => 'Gibraltar'],
    83 => ['code' => 'GK', 'name' => 'Guernsey'],
    84 => ['code' => 'GR', 'name' => 'Greece'],
    85 => ['code' => 'GL', 'name' => 'Greenland'],
    86 => ['code' => 'GD', 'name' => 'Grenada'],
    87 => ['code' => 'GP', 'name' => 'Guadeloupe'],
    88 => ['code' => 'GU', 'name' => 'Guam'],
    89 => ['code' => 'GT', 'name' => 'Guatemala'],
    90 => ['code' => 'GN', 'name' => 'Guinea'],
    91 => ['code' => 'GW', 'name' => 'Guinea-Bissau'],
    92 => ['code' => 'GY', 'name' => 'Guyana'],
    93 => ['code' => 'HT', 'name' => 'Haiti'],
    94 => ['code' => 'HM', 'name' => 'Heard and Mc Donald Islands'],
    95 => ['code' => 'HN', 'name' => 'Honduras'],
    96 => ['code' => 'HK', 'name' => 'Hong Kong'],
    97 => ['code' => 'HU', 'name' => 'Hungary'],
    98 => ['code' => 'IS', 'name' => 'Iceland'],
    99 => ['code' => 'IN', 'name' => 'India'],
    100 => ['code' => 'IM', 'name' => 'Isle of Man'],
    101 => ['code' => 'ID', 'name' => 'Indonesia'],
    102 => ['code' => 'IR', 'name' => 'Iran (Islamic Republic of)'],
    103 => ['code' => 'IQ', 'name' => 'Iraq'],
    104 => ['code' => 'IE', 'name' => 'Ireland'],
    105 => ['code' => 'IL', 'name' => 'Israel'],
    106 => ['code' => 'IT', 'name' => 'Italy'],
    107 => ['code' => 'CI', 'name' => 'Ivory Coast'],
    108 => ['code' => 'JE', 'name' => 'Jersey'],
    109 => ['code' => 'JM', 'name' => 'Jamaica'],
    110 => ['code' => 'JP', 'name' => 'Japan'],
    111 => ['code' => 'JO', 'name' => 'Jordan'],
    112 => ['code' => 'KZ', 'name' => 'Kazakhstan'],
    113 => ['code' => 'KE', 'name' => 'Kenya'],
    114 => ['code' => 'KI', 'name' => 'Kiribati'],
    115 => ['code' => 'KP', 'name' => 'Korea, Democratic People\'s Republic of'],
    116 => ['code' => 'KR', 'name' => 'Korea, Republic of'],
    117 => ['code' => 'XK', 'name' => 'Kosovo'],
    118 => ['code' => 'KW', 'name' => 'Kuwait'],
    119 => ['code' => 'KG', 'name' => 'Kyrgyzstan'],
    120 => ['code' => 'LA', 'name' => 'Lao People\'s Democratic Republic'],
    121 => ['code' => 'LV', 'name' => 'Latvia'],
    122 => ['code' => 'LB', 'name' => 'Lebanon'],
    123 => ['code' => 'LS', 'name' => 'Lesotho'],
    124 => ['code' => 'LR', 'name' => 'Liberia'],
    125 => ['code' => 'LY', 'name' => 'Libyan Arab Jamahiriya'],
    126 => ['code' => 'LI', 'name' => 'Liechtenstein'],
    127 => ['code' => 'LT', 'name' => 'Lithuania'],
    128 => ['code' => 'LU', 'name' => 'Luxembourg'],
    129 => ['code' => 'MO', 'name' => 'Macau'],
    130 => ['code' => 'MK', 'name' => 'Macedonia'],
    131 => ['code' => 'MG', 'name' => 'Madagascar'],
    132 => ['code' => 'MW', 'name' => 'Malawi'],
    133 => ['code' => 'MY', 'name' => 'Malaysia'],
    134 => ['code' => 'MV', 'name' => 'Maldives'],
    135 => ['code' => 'ML', 'name' => 'Mali'],
    136 => ['code' => 'MT', 'name' => 'Malta'],
    137 => ['code' => 'MH', 'name' => 'Marshall Islands'],
    138 => ['code' => 'MQ', 'name' => 'Martinique'],
    139 => ['code' => 'MR', 'name' => 'Mauritania'],
    140 => ['code' => 'MU', 'name' => 'Mauritius'],
    141 => ['code' => 'TY', 'name' => 'Mayotte'],
    142 => ['code' => 'MX', 'name' => 'Mexico'],
    143 => ['code' => 'FM', 'name' => 'Micronesia, Federated States of'],
    144 => ['code' => 'MD', 'name' => 'Moldova, Republic of'],
    145 => ['code' => 'MC', 'name' => 'Monaco'],
    146 => ['code' => 'MN', 'name' => 'Mongolia'],
    147 => ['code' => 'ME', 'name' => 'Montenegro'],
    148 => ['code' => 'MS', 'name' => 'Montserrat'],
    149 => ['code' => 'MA', 'name' => 'Morocco'],
    150 => ['code' => 'MZ', 'name' => 'Mozambique'],
    151 => ['code' => 'MM', 'name' => 'Myanmar'],
    152 => ['code' => 'NA', 'name' => 'Namibia'],
    153 => ['code' => 'NR', 'name' => 'Nauru'],
    154 => ['code' => 'NP', 'name' => 'Nepal'],
    155 => ['code' => 'NL', 'name' => 'Netherlands'],
    156 => ['code' => 'AN', 'name' => 'Netherlands Antilles'],
    157 => ['code' => 'NC', 'name' => 'New Caledonia'],
    158 => ['code' => 'NZ', 'name' => 'New Zealand'],
    159 => ['code' => 'NI', 'name' => 'Nicaragua'],
    160 => ['code' => 'NE', 'name' => 'Niger'],
    161 => ['code' => 'NG', 'name' => 'Nigeria'],
    162 => ['code' => 'NU', 'name' => 'Niue'],
    163 => ['code' => 'NF', 'name' => 'Norfolk Island'],
    164 => ['code' => 'MP', 'name' => 'Northern Mariana Islands'],
    165 => ['code' => 'NO', 'name' => 'Norway'],
    166 => ['code' => 'OM', 'name' => 'Oman'],
    167 => ['code' => 'PK', 'name' => 'Pakistan'],
    168 => ['code' => 'PW', 'name' => 'Palau'],
    169 => ['code' => 'PS', 'name' => 'Palestine'],
    170 => ['code' => 'PA', 'name' => 'Panama'],
    171 => ['code' => 'PG', 'name' => 'Papua New Guinea'],
    172 => ['code' => 'PY', 'name' => 'Paraguay'],
    173 => ['code' => 'PE', 'name' => 'Peru'],
    174 => ['code' => 'PH', 'name' => 'Philippines'],
    175 => ['code' => 'PN', 'name' => 'Pitcairn'],
    176 => ['code' => 'PL', 'name' => 'Poland'],
    177 => ['code' => 'PT', 'name' => 'Portugal'],
    178 => ['code' => 'PR', 'name' => 'Puerto Rico'],
    179 => ['code' => 'QA', 'name' => 'Qatar'],
    180 => ['code' => 'RE', 'name' => 'Reunion'],
    181 => ['code' => 'RO', 'name' => 'Romania'],
    182 => ['code' => 'RU', 'name' => 'Russian Federation'],
    183 => ['code' => 'RW', 'name' => 'Rwanda'],
    184 => ['code' => 'KN', 'name' => 'Saint Kitts and Nevis'],
    185 => ['code' => 'LC', 'name' => 'Saint Lucia'],
    186 => ['code' => 'VC', 'name' => 'Saint Vincent and the Grenadines'],
    187 => ['code' => 'WS', 'name' => 'Samoa'],
    188 => ['code' => 'SM', 'name' => 'San Marino'],
    189 => ['code' => 'ST', 'name' => 'Sao Tome and Principe'],
    190 => ['code' => 'SA', 'name' => 'Saudi Arabia'],
    191 => ['code' => 'SN', 'name' => 'Senegal'],
    192 => ['code' => 'RS', 'name' => 'Serbia'],
    193 => ['code' => 'SC', 'name' => 'Seychelles'],
    194 => ['code' => 'SL', 'name' => 'Sierra Leone'],
    195 => ['code' => 'SG', 'name' => 'Singapore'],
    196 => ['code' => 'SK', 'name' => 'Slovakia'],
    197 => ['code' => 'SI', 'name' => 'Slovenia'],
    198 => ['code' => 'SB', 'name' => 'Solomon Islands'],
    199 => ['code' => 'SO', 'name' => 'Somalia'],
    200 => ['code' => 'ZA', 'name' => 'South Africa'],
    201 => ['code' => 'GS', 'name' => 'South Georgia South Sandwich Islands'],
    202 => ['code' => 'ES', 'name' => 'Spain'],
    203 => ['code' => 'LK', 'name' => 'Sri Lanka'],
    204 => ['code' => 'SH', 'name' => 'St. Helena'],
    205 => ['code' => 'PM', 'name' => 'St. Pierre and Miquelon'],
    206 => ['code' => 'SD', 'name' => 'Sudan'],
    207 => ['code' => 'SR', 'name' => 'Suriname'],
    208 => ['code' => 'SJ', 'name' => 'Svalbard and Jan Mayen Islands'],
    209 => ['code' => 'SZ', 'name' => 'Swaziland'],
    210 => ['code' => 'SE', 'name' => 'Sweden'],
    211 => ['code' => 'CH', 'name' => 'Switzerland'],
    212 => ['code' => 'SY', 'name' => 'Syrian Arab Republic'],
    213 => ['code' => 'TW', 'name' => 'Taiwan'],
    214 => ['code' => 'TJ', 'name' => 'Tajikistan'],
    215 => ['code' => 'TZ', 'name' => 'Tanzania, United Republic of'],
    216 => ['code' => 'TH', 'name' => 'Thailand'],
    217 => ['code' => 'TG', 'name' => 'Togo'],
    218 => ['code' => 'TK', 'name' => 'Tokelau'],
    219 => ['code' => 'TO', 'name' => 'Tonga'],
    220 => ['code' => 'TT', 'name' => 'Trinidad and Tobago'],
    221 => ['code' => 'TN', 'name' => 'Tunisia'],
    222 => ['code' => 'TR', 'name' => 'Turkey'],
    223 => ['code' => 'TM', 'name' => 'Turkmenistan'],
    224 => ['code' => 'TC', 'name' => 'Turks and Caicos Islands'],
    225 => ['code' => 'TV', 'name' => 'Tuvalu'],
    226 => ['code' => 'UG', 'name' => 'Uganda'],
    227 => ['code' => 'UA', 'name' => 'Ukraine'],
    228 => ['code' => 'AE', 'name' => 'United Arab Emirates'],
    229 => ['code' => 'GB', 'name' => 'United Kingdom'],
    230 => ['code' => 'US', 'name' => 'United States'],
    231 => ['code' => 'UM', 'name' => 'United States minor outlying islands'],
    232 => ['code' => 'UY', 'name' => 'Uruguay'],
    233 => ['code' => 'UZ', 'name' => 'Uzbekistan'],
    234 => ['code' => 'VU', 'name' => 'Vanuatu'],
    235 => ['code' => 'VA', 'name' => 'Vatican City State'],
    236 => ['code' => 'VE', 'name' => 'Venezuela'],
    237 => ['code' => 'VN', 'name' => 'Vietnam'],
    238 => ['code' => 'VG', 'name' => 'Virgin Islands (British)'],
    239 => ['code' => 'VI', 'name' => 'Virgin Islands (U.S.)'],
    240 => ['code' => 'WF', 'name' => 'Wallis and Futuna Islands'],
    241 => ['code' => 'EH', 'name' => 'Western Sahara'],
    242 => ['code' => 'YE', 'name' => 'Yemen'],
    243 => ['code' => 'ZR', 'name' => 'Zaire'],
    244 => ['code' => 'ZM', 'name' => 'Zambia'],
    245 => ['code' => 'ZW', 'name' => 'Zimbabwe'],
];

$config['marital_status'] = [
    1 => 'I am single',
    2 => 'I am married/remarried',
    3 => 'I am separated',
    4 => 'I am divorced or widowed'
];

$config['degree'] = [
    1 => ['en' => 'Bachelor'],
    2 => ['en' => 'Master'],
    3 => ['en' => 'Doctoral']
];
