<?php

namespace controller;

use libs\utils as utils;
use model\users as users;

defined("__MAIN__") or exit;

/**
 * Class pages
 *
 * controls login, register, profile and logout pages
 *
 */
class pages {

    private $page = '';
    private $error = null;
    private $lang = 'ru';

    public function __construct() {
        $this->lang = utils::http_var("lang");
    }

    /**
     * Method route
     *
     * Catches requested pg (page) param and according to that param
     * loads pages from views folder
     *
     * @param (void)
     * @return (void)
     */
    public function route() {
        $this->page = utils::http_var("pg");

        if ($this->page == 'ajax') {
            $this->call_ajax();
        } else if ($this->page == "register") {
            $this->call_register();
        } else if ($this->page == 'restore_password') {
            $this->call_restore_password();
        } else if (!users::i()->setUserInfo(users::i()->is_logged_in())) {
            $this->call_login();
        } else if ($this->page == 'login') {
            utils::redirect_to("/");
        } else {
            switch ($this->page) {
                case "":
                case "profile":
                    $this->call_profile();
                    break;
                case "logout":
                    $this->call_logout();
                    break;
                case "media":
                    $this->call_media();
                    break;
            }
        }

        $this->load_view();
    }

    private function call_profile() {
        if (utils::http_var("change_password")) {
            try {
                users::i()->change_password(utils::http_var("current_password"), utils::http_var("new_password"));
                utils::reload_page();
            } catch (\Exception $ex) {
                $this->error = $ex->getMessage();
            }
        }
        $this->page = "my_profile";
    }

    private function call_logout() {
        users::i()->logout();
        utils::redirect_to("./");
    }

    private function call_login() {
        if (utils::http_var("login")) {
            try {
                users::i()->login(utils::http_var("username"), utils::http_var("password"));
                utils::redirect_to("./");
            } catch (\Exception $ex) {
                $this->error = $ex->getMessage();
            }
        }

        $this->page = "login";
    }

    private function call_register() {
        
    }

    private function call_ajax() {
        switch (utils::http_var("method")) {
            case "user_exists":
                $this->call_ajax_user_exists();
                break;
            case "register":
                $this->call_ajax_register();
                break;
        }

        exit;
    }

    private function call_ajax_register() {
        $json = [
            'status' => 200,
            'error' => '',
            'data' => 0
        ];

        try {
            \libs\mydb::i()->beginTransaction();
            $json['data'] = (int) users::i()->register([
                        "firstname" => utils::http_var("firstname"),
                        "lastname" => utils::http_var("lastname"),
                        "patronymic" => utils::http_var("patronymic"),
                        "birthdate" => utils::http_var("birthdate"),
                        "marital_status" => utils::http_var("marital_status"),
                        "country" => utils::http_var("country"),
                        "city" => utils::http_var("city"),
                        "address" => utils::http_var("address"),
                        "edu_country" => utils::http_var("edu_country"),
                        "edu_city" => utils::http_var("edu_city"),
                        "university" => utils::http_var("university"),
                        "department" => utils::http_var("department"),
                        "degree" => utils::http_var("degree"),
                        "edu_start_year" => utils::http_var("edu_start_year"),
                        "edu_end_year" => utils::http_var("edu_end_year"),
                        "work_country" => utils::http_var("work_country"),
                        "work_city" => utils::http_var("work_city"),
                        "company" => utils::http_var("company"),
                        "position" => utils::http_var("position"),
                        "work_start_year" => utils::http_var("work_start_year"),
                        "work_end_year" => utils::http_var("work_end_year"),
                        "working" => utils::http_var("working"),
                        "phone" => utils::http_var("phone"),
                        "email" => utils::http_var("email"),
                        "linkedin" => utils::http_var("linkedin"),
                        "github" => utils::http_var("github"),
                        "additional_information" => utils::http_var("additional_information"),
                        "username" => utils::http_var("username"),
                        "password" => utils::http_var("password"),
                        "photo" => $_FILES['photo']
            ]);
            \libs\mydb::i()->commit();
        } catch (\Exception $ex) {
            \libs\mydb::i()->rollBack();
            $json['status'] = 500;
            $json['error'] = json_decode($ex->getMessage(), true);
        }

        echo json_encode($json);
    }

    private function call_ajax_user_exists() {
        $json = [
            'status' => 200,
            'error' => '',
            'data' => 0
        ];

        try {
            $json['data'] = (int) users::i()->existsUser([
                        'username' => utils::http_var('username')
            ]);
        } catch (\Exception $ex) {
            $json['status'] = 500;
            $json['error'] = $ex->getMessage();
        }

        echo json_encode($json);
    }

    private function call_media() {
        switch (utils::http_var("type")) {
            case "profile":
                try {
                    $res = users::i()->get_profile_photo();
                    header($res['header']);
                    echo $res['content'];
                } catch (\Exception $ex) {
                    echo $ex->getMessage();
                }
                break;
        }

        exit;
    }

    /**
     * Method load_view
     *
     * Loads pages from view folder according to $this->page attribute value
     *
     * @param (void)
     * @return (void)
     */
    private function load_view() {
        global $config, $lang;

        try {
            if (users::i()->is_logged_in()) {
                $user = users::i()->getUserInfo();
                $education = users::i()->get_education();
                $work_experience = users::i()->get_work_experience();
            }
        } catch (\Exception $ex) {
            
        }

        if (file_exists(__DIR__ . "/../../views/v_{$this->page}.php"))
            require_once __DIR__ . "/../../views/v_{$this->page}.php";
        else
            require_once __DIR__ . "/../../views/v_notfound.php";
    }

}
