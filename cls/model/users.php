<?php

namespace model;

defined("__MAIN__") or exit;

use libs\mydb as mydb;

class users {

    private static $instance;
    private $userInfo = [];

    public static function i() {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function __construct() {
        
    }

    public function get_education() {
        $strSQL = "select * from education where userid = :id";
        $stmt = mydb::i()->prepare($strSQL);
        $pdoParams = [
            ':id' => $this->getUserInfo()['id']
        ];
        if (!$stmt->execute($pdoParams)) {
            list(, $errCode, $errMsg) = $stmt->errorInfo();

            switch ($errCode) {
                default:
                    throw new Exception("Server error. Try again!");
            }
        }
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function get_work_experience() {
        $strSQL = "select * from work_experience where userid = :id";
        $stmt = mydb::i()->prepare($strSQL);
        $pdoParams = [
            ':id' => $this->getUserInfo()['id']
        ];
        if (!$stmt->execute($pdoParams)) {
            list(, $errCode, $errMsg) = $stmt->errorInfo();

            switch ($errCode) {
                default:
                    throw new Exception("Server error. Try again!");
            }
        }
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function get_profile_photo() {
        $strSQL = "select photo from user where id = :id";
        $stmt = mydb::i()->prepare($strSQL);
        $pdoParams = [
            ':id' => $this->getUserInfo()['id']
        ];
        if (!$stmt->execute($pdoParams)) {
            list(, $errCode, $errMsg) = $stmt->errorInfo();

            switch ($errCode) {
                default:
                    throw new Exception("Server error. Try again!");
            }
        }
        
        $img = $stmt->fetch(\PDO::FETCH_ASSOC)['photo'];
        if(!$img)
            $img = file_get_contents(__DIR__ . '/../../views/assets/images/noimage.jpg');

        return ['header' => 'Content-type: image/jpeg', 'content' => $img];
    }

    public function setUserInfo($userInfo) {
        if (!$userInfo)
            return false;

        $this->userInfo = $userInfo;
        return true;
    }

    public function getUserInfo() {
        return $this->userInfo;
    }

    public function existsUser($params = []) {
        $strSQL = "select count(id) cnt from user where ";
        $pdoParams = [];
        $hasField = false;

        if (!empty($params['user_id'])) {
            $strSQL .= ($hasField ? " and " : "") . " id = :id ";
            $pdoParams[':id'] = $params['user_id'];
            $hasField = true;
        }

        if (!empty($params['token'])) {
            $strSQL .= ($hasField ? " and " : "") . " token = :token ";
            $pdoParams[':token'] = $params['token'];
            $hasField = true;
        }

        if (!empty($params['username'])) {
            $strSQL .= ($hasField ? " and " : "") . " username = :username ";
            $pdoParams[':username'] = $params['username'];
            $hasField = true;
        }

        $stmt = mydb::i()->prepare($strSQL);

        if (!$stmt->execute($pdoParams))
            return false;

        return $stmt->fetch(\PDO::FETCH_ASSOC)['cnt'] > 0;
    }

    public function register($params = []) {
        global $config, $lang;

        $firstname = !empty($params["firstname"]) ? trim($params["firstname"]) : null;
        $lastname = !empty($params["lastname"]) ? trim($params["lastname"]) : null;
        $patronymic = !empty($params["patronymic"]) ? trim($params["patronymic"]) : null;
        $birthdate = !empty($params["birthdate"]) ? trim($params["birthdate"]) : null;
        $marital_status = !empty($params["marital_status"]) ? trim($params["marital_status"]) : null;
        $country = !empty($params["country"]) ? (int) $params["country"] : null;
        $city = !empty($params["city"]) ? trim($params["city"]) : null;
        $address = !empty($params["address"]) ? trim($params["address"]) : null;
        $edu_country = !empty($params["edu_country"]) ? (int) $params["edu_country"] : null;
        $edu_city = !empty($params["edu_city"]) ? trim($params["edu_city"]) : null;
        $university = !empty($params["university"]) ? trim($params["university"]) : null;
        $department = !empty($params["department"]) ? trim($params["department"]) : null;
        $degree = !empty($params["degree"]) ? (int) $params["degree"] : null;
        $edu_start_year = !empty($params["edu_start_year"]) ? (int) $params["edu_start_year"] : null;
        $edu_end_year = !empty($params["edu_end_year"]) ? (int) $params["edu_end_year"] : null;
        $work_country = !empty($params["work_country"]) ? (int) $params["work_country"] : null;
        $work_city = !empty($params["work_city"]) ? trim($params["work_city"]) : null;
        $company = !empty($params["company"]) ? trim($params["company"]) : null;
        $position = !empty($params["position"]) ? trim($params["position"]) : null;
        $work_start_year = !empty($params["work_start_year"]) ? (int) $params["work_start_year"] : null;
        $work_end_year = !empty($params["work_end_year"]) ? (int) $params["work_end_year"] : null;
        $working = !empty($params['working']) ? true : false;
        $phone = !empty($params["phone"]) ? trim($params["phone"]) : null;
        $email = !empty($params["email"]) ? trim($params["email"]) : null;
        $linkedin = !empty($params["linkedin"]) ? trim($params["linkedin"]) : null;
        $github = !empty($params["github"]) ? trim($params["github"]) : null;
        $additional_information = !empty($params["additional_information"]) ? trim($params["additional_information"]) : null;
        $username = !empty($params["username"]) ? trim($params["username"]) : null;
        $password = !empty($params["password"]) ? $params["password"] : null;
        $photo = !empty($params["photo"]) ? $params["photo"] : null;

        if (!$firstname)
            throw new \Exception(json_encode([
                'step' => 0,
                'field' => 'firstname',
                'error' => $lang['firstname_is_empty']
            ]));

        if (!$lastname)
            throw new \Exception(json_encode([
                'step' => 0,
                'field' => 'lastname',
                'error' => $lang['lastname_is_empty']
            ]));

        if (!$patronymic)
            throw new \Exception(json_encode([
                'step' => 0,
                'field' => 'patronymic',
                'error' => $lang['patronymic_is_empty']
            ]));

        if (!$patronymic)
            throw new \Exception(json_encode([
                'step' => 0,
                'field' => 'birthdate',
                'error' => $lang['birthdate_is_empty']
            ]));

        if (!isset($config['marital_status'][$marital_status]))
            throw new \Exception(json_encode([
                'step' => 0,
                'field' => 'marital_status',
                'error' => $lang['marital_status_select']
            ]));

        if (!isset($config['countries'][$country]))
            throw new \Exception(json_encode([
                'step' => 1,
                'field' => 'country',
                'error' => $lang['country_select']
            ]));

        if (!$city)
            throw new \Exception(json_encode([
                'step' => 1,
                'field' => 'city',
                'error' => $lang['city_placeholder']
            ]));

        if (!$address)
            throw new \Exception(json_encode([
                'step' => 1,
                'field' => 'address',
                'error' => $lang['address_placeholder']
            ]));

        if (!isset($config['countries'][$country]))
            throw new \Exception(json_encode([
                'step' => 2,
                'field' => 'edu_country',
                'error' => $lang['country_select']
            ]));

        if (!$edu_city)
            throw new \Exception(json_encode([
                'step' => 2,
                'field' => 'edu_city',
                'error' => $lang['city_placeholder']
            ]));

        if (!$university)
            throw new \Exception(json_encode([
                'step' => 2,
                'field' => 'university',
                'error' => $lang['university_placeholder']
            ]));

        if (!$department)
            throw new \Exception(json_encode([
                'step' => 2,
                'field' => 'department',
                'error' => $lang['department_placeholder']
            ]));

        if (!isset($config['degree'][$degree]))
            throw new \Exception(json_encode([
                'step' => 2,
                'field' => 'degree',
                'error' => $lang['degree_placeholder']
            ]));

        if (!$edu_start_year)
            throw new \Exception(json_encode([
                'step' => 2,
                'field' => 'edu_start_year',
                'error' => $lang['start_year_placeholder']
            ]));

        if (!$edu_end_year)
            throw new \Exception(json_encode([
                'step' => 2,
                'field' => 'edu_end_year',
                'error' => $lang['end_year_placeholder']
            ]));

        if (!isset($config['countries'][$work_country]))
            throw new \Exception(json_encode([
                'step' => 3,
                'field' => 'work_country',
                'error' => $lang['country_select']
            ]));

        if (!$work_city)
            throw new \Exception(json_encode([
                'step' => 3,
                'field' => 'work_city',
                'error' => $lang['city_placeholder']
            ]));

        if (!$company)
            throw new \Exception(json_encode([
                'step' => 3,
                'field' => 'company',
                'error' => $lang['company_placeholder']
            ]));

        if (!$work_start_year)
            throw new \Exception(json_encode([
                'step' => 3,
                'field' => 'work_start_year',
                'error' => $lang['start_year_placeholder']
            ]));

        if (!$working && !$work_end_year)
            throw new \Exception(json_encode([
                'step' => 3,
                'field' => 'end_year',
                'error' => $lang['end_year_placeholder']
            ]));

        if (!$phone)
            throw new \Exception(json_encode([
                'step' => 4,
                'field' => 'phone',
                'error' => $lang['phone_placeholder']
            ]));

        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            throw new \Exception(json_encode([
                'step' => 4,
                'field' => 'email',
                'error' => $lang['email_placeholder']
            ]));

        if ($linkedin && !filter_var($linkedin, FILTER_VALIDATE_URL))
            throw new \Exception(json_encode([
                'step' => 4,
                'field' => 'linkedin',
                'error' => $lang['incorrect_url']
            ]));

        if ($github && !filter_var($github, FILTER_VALIDATE_URL))
            throw new \Exception(json_encode([
                'step' => 4,
                'field' => 'github',
                'error' => $lang['incorrect_url']
            ]));

        if (!$username)
            throw new \Exception(json_encode([
                'step' => 6,
                'field' => 'username',
                'error' => $lang['username_placeholder']
            ]));

        if ($this->existsUser(['username' => $username]))
            throw new \Exception(json_encode([
                'step' => 6,
                'field' => 'username',
                'error' => $lang['username_exists']
            ]));

        if (!$password)
            throw new \Exception(json_encode([
                'step' => 6,
                'field' => 'password',
                'error' => $lang['password_placeholder']
            ]));


        // create new user
        $strSQL = "insert into user(token,username,password,firstname,lastname,patronymic,birthdate,marital_status,country,city,address,phone,email,linkedin,github,about_me)"
                . "values(:token,:username,:password,:firstname,:lastname,:patronymic,:birthdate,:marital_status,:country,:city,:address,:phone,:email,:linkedin,:github,:about_me)";

        $stmt = mydb::i()->prepare($strSQL);
        $pdoParams = [
            ":username" => $username,
            ":password" => password_hash($password, PASSWORD_BCRYPT),
            ":firstname" => $firstname,
            ":lastname" => $lastname,
            ":patronymic" => $patronymic,
            ":birthdate" => $birthdate,
            ":marital_status" => $marital_status,
            ":country" => $country,
            ":city" => $city,
            ":address" => $address,
            ":phone" => $phone,
            ":email" => $email,
            ":linkedin" => $linkedin,
            ":github" => $github,
            ":about_me" => $additional_information,
            ":token" => md5(uniqid())
        ];

        if (!$stmt->execute($pdoParams)) {
            list(, $errCode, $errMsg) = $stmt->errorInfo();

            switch ($errCode) {
                case 1062:
                    if (strstr($errMsg, "email")) {
                        throw new \Exception(json_encode([
                            'step' => 4,
                            'field' => 'email',
                            'error' => 'This email belongs to another user. Enter different email'
                        ]));
                    }
                    break;
                default:
                    throw new \Exception(json_encode([
                        'step' => 6,
                        'field' => 'password',
                        'error' => 'Server error. Try again!'
                    ]));
            }
        }

        $userid = mydb::i()->lastInsertId();

        // create education
        $strSQL = "insert into education(userid,country,city,university,department,degree,start_year,end_year)"
                . "values(:userid,:country,:city,:university,:department,:degree,:start_year,:end_year)";

        $stmt = mydb::i()->prepare($strSQL);
        $pdoParams = [
            ':userid' => $userid,
            ':country' => $edu_country,
            ':city' => $edu_city,
            ':university' => $university,
            ':department' => $department,
            ':degree' => $degree,
            ':start_year' => $edu_start_year,
            ':end_year' => $edu_end_year,
        ];

        if (!$stmt->execute($pdoParams)) {
            list(, $errCode, $errMsg) = $stmt->errorInfo();

            switch ($errCode) {
                default:
                    throw new \Exception(json_encode([
                        'step' => 6,
                        'field' => 'password',
                        'error' => 'Server error. Try again!'
                    ]));
            }
        }

        // create work experience
        $strSQL = "insert into work_experience(userid,country,city,company,position,start_year,end_year,working)"
                . "values(:userid,:country,:city,:company,:position,:start_year,:end_year,:working)";

        $stmt = mydb::i()->prepare($strSQL);
        $pdoParams = [
            ':userid' => $userid,
            ':country' => $work_country,
            ':city' => $work_city,
            ':company' => $company,
            ':position' => $position,
            ':start_year' => $work_start_year,
            ':end_year' => $work_end_year,
            ':working' => (int) $working
        ];

        if (!$stmt->execute($pdoParams)) {
            list(, $errCode, $errMsg) = $stmt->errorInfo();

            switch ($errCode) {
                default:
                    throw new \Exception(json_encode([
                        'step' => 6,
                        'field' => 'password',
                        'error' => 'Server error. Try again!'
                    ]));
            }
        }

        try {
            // upload profile photo
            $this->update_profile_photo($userid, $photo);
        } catch (\Exception $ex) {
            throw new \Exception(json_encode([
                'step' => 0,
                'field' => 'photo',
                'error' => $ex->getMessage()
            ]));
        }
    }

    private function update_profile_photo($userid, $photo) {
        global $config, $lang;

        if ($photo['error'])
            return;
        //throw new \Exception($lang['file_error']);

        if ($photo['size'] > $config['profile_photo']['size'])
            throw new \Exception($lang['file_size_error'] . $photo['size_str']);

        if (!preg_match("/^image\//", mime_content_type($photo['tmp_name'])))
            throw new \Exception($lang['file_type_error']);

        list($width, $height, $type) = getimagesize($photo['tmp_name']);

        if (!isset($config['allowed_media_types'][$type]))
            throw new \Exception($lang['file_type_error']);

        if ($width != $config['profile_photo']['width'] || $height != $config['profile_photo']['height'])
            throw new \Exception("{$lang['file_dim_error']} {$config['profile_photo']['width']}x{$config['profile_photo']['height']}");

        $strSQL = "update user set photo = :photo where id = :id";
        $pdoParams = [
            ':id' => $userid,
            ':photo' => file_get_contents($photo['tmp_name'])
        ];
        $stmt = mydb::i()->prepare($strSQL);

        if (!$stmt->execute($pdoParams)) {
            list(, $errCode, $errMsg) = $stmt->errorInfo();

            switch ($errCode) {
                default:
                    throw new \Exception("Server error. Try again!");
            }
        }

        return true;
    }

    public function change_password($current_password, $new_password) {
        global $config, $lang;

        if (!trim($current_password) || !trim($new_password))
            throw new \Exception($lang['type_all_passwords']);

        if (!password_verify($current_password, $this->getUserInfo()['password']))
            throw new \Exception($lang['current_password_error']);

        $token = md5(uniqid());
        $new_password = password_hash($new_password, PASSWORD_BCRYPT);

        $strSQL = "update user set token = :token, password = :pass where id = :id";
        $stmt = mydb::i()->prepare($strSQL);
        $pdoParams = [
            ':id' => $this->getUserInfo()['id'],
            ':token' => $token,
            ':pass' => $new_password
        ];

        if (!$stmt->execute($pdoParams)) {
            list(, $errCode, $errMsg) = $stmt->errorInfo();

            switch ($errCode) {
                default:
                    throw new \Exception("Server error. Try again!");
            }
        }

        $this->userInfo['token'] = $token;
        $this->userInfo['password'] = $new_password;

        return self::set_cookie($this->getUserInfo()['id'], $token);
    }

    public function login($username, $pass, $remember = true) {
        global $config, $lang;

        if (!trim($username))
            throw new \Exception($lang['username_placeholder']);

        if (!trim($pass))
            throw new \Exception($lang['password_placeholder']);

        $strSQL = "select * from user where username = :username or email = :username";
        $stmt = mydb::i()->prepare($strSQL);
        $pdoParams = [
            ':username' => $username
        ];

        if (!$stmt->execute($pdoParams)) {
            list(, $errCode, $errMsg) = $stmt->errorInfo();

            switch ($errCode) {
                default:
                    throw new \Exception("Server error. Try again!");
            }
        }

        if ($stmt->rowCount() < 1)
            throw new \Exception($lang['credential_error']);

        $user = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (!password_verify($pass, $user['password']))
            throw new \Exception($lang['credential_error']);

        $this->setUserInfo($user);

        return self::set_cookie($user['id'], $user['token']);
    }

    public static function logout() {
        if (isset($_COOKIE[CONF_SESS_COOKIE_NAME])) {
            unset($_COOKIE[CONF_SESS_COOKIE_NAME]);
            return setcookie(CONF_SESS_COOKIE_NAME, NULL, -1, "/", CONF_SESS_DOMAIN);
        }

        return true;
    }

    public function is_logged_in() {
        if (empty($_COOKIE[CONF_SESS_COOKIE_NAME]))
            return false;

        $cookie = $_COOKIE[CONF_SESS_COOKIE_NAME];
        $cookie = preg_split("/:/", $cookie);
        if (count($cookie) != 2)
            return false;

        $strSQL = "select u.* from user u "
                . "where u.id = :id "
                . "and u.token = :token";
        $pdoParams = [
            ':id' => $cookie[0],
            ':token' => $cookie[1]
        ];

        $stmt = mydb::i()->prepare($strSQL);
        if (!$stmt->execute($pdoParams)) {
            list(, $errCode, $errMsg) = $stmt->errorInfo();

            switch ($errCode) {
                default:
                    return false;
            }
        }

        if ($stmt->rowCount() < 1) {
            return false;
        }

        self::update_cookie_life_time();
        return $this->userInfo = $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    private static function update_cookie_life_time() {
        if (!isset($_COOKIE[CONF_SESS_COOKIE_NAME]))
            return false;

        $val = $_COOKIE[CONF_SESS_COOKIE_NAME];
        return setcookie(CONF_SESS_COOKIE_NAME, $val, time() + CONF_SESS_EXPIRE_TIME, "/", CONF_SESS_DOMAIN);
    }

    public static function change_session($user) {
        return setcookie(CONF_SESS_COOKIE_NAME, $user['id'] . ":" . $user['token'], time() + CONF_SESS_EXPIRE_TIME, "/", CONF_SESS_DOMAIN);
    }

    public static function set_cookie($userId, $token, $remember = true) {
        if ($remember)
            return setcookie(CONF_SESS_COOKIE_NAME, $userId . ":" . $token, time() + CONF_SESS_EXPIRE_TIME, "/", CONF_SESS_DOMAIN);
        else
            return setcookie(CONF_SESS_COOKIE_NAME, $userId . ":" . $token, 0, "/", CONF_SESS_DOMAIN);
    }

}
