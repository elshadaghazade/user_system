<?php

namespace libs;

/**
 * Description of mydb
 *
 * @author Elshad
 */


class mydb extends \PDO {

    private static $instance;

    public static function i() {
        global $config;


        if (!(self::$instance instanceof self)) {

            $dns = "mysql:dbname=" . $config['sql_db'] . ";host=" . $config['sql_host'] . ";port=" . $config['sql_port'];

            $options = [
                \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"
            ];

            self::$instance = new self($dns, $config['sql_user'], $config['sql_pass'], $options);
        }

        return self::$instance;
    }

    public function __construct($dsn, $username = null, $password = null, $options = null) {
        parent::__construct($dsn, $username, $password, $options);
    }

}
