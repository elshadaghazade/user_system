<?php

namespace libs;

defined("__MAIN__") or exit;

class utils {

    public static function reload_page() {
        header("Location: " . $_SERVER['REQUEST_URI']);
        exit;
    }

    public static function redirect_to($url, $status = 301) {
        header("Location: {$url}", $status);
        exit;
    }

    public static function http_var($var = '', $default = '', $type = 'str') {
        if (isset($_REQUEST[$var])) {
            if (is_array($_REQUEST[$var]))
                return $_REQUEST[$var];
            if (trim($_REQUEST[$var])) {
                if (!empty($type)) {
                    if ($type == 'int')
                        return intval($_REQUEST[$var]);
                    else if ($type == 'float')
                        return floatval($_REQUEST[$var]);
                    return trim($_REQUEST[$var]);
                } else {
                    return trim($_REQUEST[$var]);
                }
            } else
                return $default;
        } else { // return default
            // check if default is given
            if (!isset($default)) {
                if (empty($type))
                    $type = 'str';
                if ($type == 'int' || $type == 'float')
                    $default = 0;
                else
                    $default = '';
            }
            return $default;
        }
    }

    public static function from_bytes($bytes, $precision = 2) {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // $bytes /= pow(1024, $pow);
        $bytes /= (1 << (10 * $pow)); 

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

}
