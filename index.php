<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define("__MAIN__", true);

require 'config.php';
$lang = 'langs/lang_' . (!empty($_GET['lang']) ? $_GET['lang'] : 'ru') . '.php';
if (file_exists($lang))
    require $lang;
else
    exit;
require 'myautoloader.php';

$pages = new controller\pages();
$pages->route();
